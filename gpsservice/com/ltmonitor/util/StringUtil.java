/*******************************************************************************
 * Copyright (c) 2005, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.ltmonitor.util;

import java.math.BigDecimal;

/**
 * @project myoa
 * @version 0.1
 * @author zhengxin
 * @created at 2006-7-4 20:57:58
 * @purpose: provide common mehtod for string operations;
 * @edited by zhengxin
 */
public class StringUtil {

	/**
	 * ת��һ��յ��ַ�
	 * 
	 * @param str
	 * @return
	 */
	public static String convertStringIfNull(String str) {
		return str == null ? StringConstants.EMPTY_STRING : str;
	}

	/**
	 * 判断是否是有效的字符串，空串为无效串
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isValidStr(String str) {
		return str != null && str.trim().length() > 0;
	}
	
	/**
	 * 将字符串的换行替换成HTML的换行符号
	 * @param str
	 * @return
	 */
	public static String formatHtmlString(String str){
		
		return str.replaceAll("\r\n", "<BR/>");
	}
	
	
	/**
	 * 将数组中的字符，连成以逗号分隔的形式
	 * 如果 obj 为null，并不抛出异常，直接返回为空
	 * @param obj
	 * @return
	 */
	public static String joinStr(Object[] obj){
		if (obj == null)
			return null;
		
		StringBuffer buffer = new StringBuffer();
		
		if (obj.length > 0);
		  buffer.append(obj[0]);
		
		for(int m = 1; m < obj.length; m++){
			buffer.append(StringConstants.COMMA).append(obj[m]);
		}
		
		return buffer.toString();
	}

	/**
	 * 根据字符串转换为布尔值ֵ
	 * 
	 * @param str
	 * @return
	 */
	public static boolean getBooleanValue(String str) {
		return isValidStr(str) ? str.toLowerCase().trim().equals(
				StringConstants.TRUE) : false;
	}

	public static int getIntValue(String str, int defaultValue) {
		try {
			return Integer.parseInt(str);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public static BigDecimal getBigDecimal(String param) {
		if (StringUtil.isValidStr(param)) {
			try {
				return new BigDecimal(param);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		
		return new BigDecimal(-1);
	}
	
	public static String describe(Object[] values){
		StringBuffer buff = new StringBuffer();
		
		for(int m = 0; m < values.length; m++){
			buff.append(values[m]).append(", ");
		}
		
		return buff.toString();
	}
	
	public static long getStrTolong(String value){
		long result=0;
		if (value==null||(value!=null && value.equals(""))) return result;
	
		try{
			result=Long.parseLong(value);
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public static double getStrTodouble(String value){
		double result=0;
		if (value==null ||(value!=null && value.equals(""))) return result;
		try{
			result=Double.parseDouble(value);
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;		
	}	

}
