package com.ltmonitor.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 * 报警督办信息
 * 
 * @author Administrator
 * 
 */
@Entity
@Table(name = "WarnMsgUrgTodoReq")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class WarnMsgUrgeTodoReq extends TenantEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int entityId;
	
	public WarnMsgUrgeTodoReq()
	{
		createDate = new Date();
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int value) {
		entityId = value;
	}
	
	private String plateNo;
	
	private int plateColor;
		

	private Integer vehicleId;

	private int warnSrc;

	private int warnType;

	private Date warnTime;

	private int supervicsionId;

	private Date supervisionEndtime;

	private int supervisionLevel;

	private String supervisor;

	private String supervisorTel;

	private String supervisorEmail;

	private Date ackTime;

	private int ackFlag;

	private int origin;

	private int result = 0;

	public Integer getVehicleId() {
		return this.vehicleId;
	}

	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	public int getWarnSrc() {
		return this.warnSrc;
	}

	public void setWarnSrc(int warnSrc) {
		this.warnSrc = warnSrc;
	}

	public int getWarnType() {
		return this.warnType;
	}

	public void setWarnType(int warnType) {
		this.warnType = warnType;
	}

	public Date getWarnTime() {
		return this.warnTime;
	}

	public void setWarnTime(Date warnTime) {
		this.warnTime = warnTime;
	}

	public int getSupervicsionId() {
		return this.supervicsionId;
	}

	public void setSupervicsionId(int supervicsionId) {
		this.supervicsionId = supervicsionId;
	}

	public Date getSupervisionEndtime() {
		return this.supervisionEndtime;
	}

	public void setSupervisionEndtime(Date supervisionEndtime) {
		this.supervisionEndtime = supervisionEndtime;
	}

	public int getSupervisionLevel() {
		return this.supervisionLevel;
	}

	public void setSupervisionLevel(int supervisionLevel) {
		this.supervisionLevel = supervisionLevel;
	}

	public String getSupervisor() {
		return this.supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getSupervisorTel() {
		return this.supervisorTel;
	}

	public void setSupervisorTel(String supervisorTel) {
		this.supervisorTel = supervisorTel;
	}

	public String getSupervisorEmail() {
		return this.supervisorEmail;
	}

	public void setSupervisorEmail(String supervisorEmail) {
		this.supervisorEmail = supervisorEmail;
	}

	public Date getAckTime() {
		return this.ackTime;
	}

	public void setAckTime(Date ackTime) {
		this.ackTime = ackTime;
	}

	public int getResult() {
		return this.result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getAckFlag() {
		return this.ackFlag;
	}

	public void setAckFlag(int ackFlag) {
		this.ackFlag = ackFlag;
	}

	public int getOrigin() {
		return this.origin;
	}

	public void setOrigin(int origin) {
		this.origin = origin;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public int getPlateColor() {
		return plateColor;
	}

	public void setPlateColor(int plateColor) {
		this.plateColor = plateColor;
	}
}

/*
 * Location: E:\goss_web\goss_service\goss_service.jar Qualified Name:
 * com.yuweitek.goss.model.WarnMsgUrgeTodoReq JD-Core Version: 0.6.0
 */