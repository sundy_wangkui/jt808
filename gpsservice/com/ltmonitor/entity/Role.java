package com.ltmonitor.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;


@Entity
@org.hibernate.annotations.Proxy(lazy = false)
@Table(name = "role")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Role extends TenantEntity {
	private static final long serialVersionUID = 3690197650654049888L;
	private int entityId;
	private String name;
	private String code; // 指定角色的编码，用于后台硬编程判断
	private Integer status;
	private Set<FuncModel> funcs = new HashSet();

	public Role() {
	}

	public Role(String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "roleId", unique = true, nullable = false)
	public int getEntityId() {
		return this.entityId;
	}

	public String getName() {
		return this.name;
	}

	public void setEntityId(int id) {
		this.entityId = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "RoleFunc", joinColumns = { @JoinColumn(name = "role_id") }, inverseJoinColumns = { @JoinColumn(name = "sys_func_id") })
	@OrderBy("menuSort ASC")
	public Set<FuncModel> getFuncs() {
		return this.funcs;
	}

	public void setFuncs(Set<FuncModel> funcs) {
		this.funcs = funcs;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
