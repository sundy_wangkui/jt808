package com.ltmonitor.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="PlatformState")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class PlatformState {
	
	public static String STATE_CONNECTED = "已建立连接";
	
	public static String STATE_DISCONNECTED = "连接断开";
	
	public static String STATE_STOP = "已终止";
	
	public static String STATE_START = "已启动";
	
	private String mainLinkState = "未建立连接";
	
	private Date mainLinkDate;
	
	private String subLinkState = "未建立连接";

	private Date subLinkDate;
	
	private String gpsServerState = "未启动";

	private Date gpsServerDate;
	
	public PlatformState()
	{
		mainLinkDate = new Date();
		subLinkDate = new Date();
		gpsServerDate = new Date();
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "stateId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}

	public String getMainLinkState() {
		return mainLinkState;
	}

	public void setMainLinkState(String mainLinkState) {
		this.mainLinkState = mainLinkState;
	}

	public Date getMainLinkDate() {
		return mainLinkDate;
	}

	public void setMainLinkDate(Date mainLinkDate) {
		this.mainLinkDate = mainLinkDate;
	}

	public String getSubLinkState() {
		return subLinkState;
	}

	public void setSubLinkState(String subLinkState) {
		this.subLinkState = subLinkState;
	}

	public Date getSubLinkDate() {
		return subLinkDate;
	}

	public void setSubLinkDate(Date subLinkDate) {
		this.subLinkDate = subLinkDate;
	}

	public String getGpsServerState() {
		return gpsServerState;
	}

	public void setGpsServerState(String gpsServerState) {
		this.gpsServerState = gpsServerState;
	}

	public Date getGpsServerDate() {
		return gpsServerDate;
	}

	public void setGpsServerDate(Date gpsServerDate) {
		this.gpsServerDate = gpsServerDate;
	}

}
