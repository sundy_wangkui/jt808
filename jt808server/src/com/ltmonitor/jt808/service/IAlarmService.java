package com.ltmonitor.jt808.service;

import com.ltmonitor.entity.GPSRealData;

public interface IAlarmService {

	//public abstract void updateOnlineTime(String simNo);

	public abstract void processRealData(GPSRealData rd);

	// 创建上线下线状态变化的记录，记录变化的起止时间和间隔，及经纬度坐标
	public abstract void createOnlineChangeRecord(GPSRealData rd, String OperateType);

	public abstract GPSRealData GetOldRealData(String plateNo);

	public abstract void stopService();

}